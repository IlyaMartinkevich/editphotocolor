import cv2 as cv
from tkinter import *

R_Color = G_Color = B_Color = 0
colorDiff = 35


def callbackR(sv):
    global R_Color
    R_Color = int(sv.get())


def callbackG(sv):
    global G_Color
    G_Color = int(sv.get())


def callbackB(sv):
    global B_Color
    B_Color = int(sv.get())


# Load the aerial image and convert to HSV colourspace
image = cv.imread("imgInput/test.jpg")

cv.imshow("imgInput", cv.imread("imgInput/test.jpg"))


def on_click(event, x, y, flags, param):
    if event == cv.EVENT_LBUTTONDOWN:
        brown_lo = image[y, x] - colorDiff
        brown_hi = image[y, x] + colorDiff

        mask = cv.inRange(image, brown_lo, brown_hi)

        image[mask > 0] = (B_Color, G_Color, R_Color)
        cv.imwrite("test.jpg", image)

        cv.imshow("imgOutput", cv.imread("test.jpg"))
        cv.destroyWindow("imgOutput")
        cv.imshow("imgOutput", cv.imread("test.jpg"))


cv.setMouseCallback("imgInput", on_click)

window = Tk()

window.geometry('350x200')
window.title("Welcome to Editor Photo")

R_Var = IntVar()
R_Var.trace("w", lambda name, index, mode, sv=R_Var: callbackR(sv))
R_Input = Entry(window, width=10, textvariable=R_Var).grid(row=0, column=1)

G_Var = IntVar()
G_Var.trace("w", lambda name, index, mode, sv=G_Var: callbackG(sv))
G_Input = Entry(window, width=10, textvariable=G_Var).grid(row=1, column=1)

B_Var = IntVar()
B_Var.trace("w", lambda name, index, mode, sv=B_Var: callbackB(sv))
B_Input = Entry(window, width=10, textvariable=B_Var).grid(row=2, column=1)

R_Label = Label(window, text="R").grid(row=0, column=0)
G_Label = Label(window, text="G").grid(row=1, column=0)
B_Label = Label(window, text="B").grid(row=2, column=0)

window.mainloop()

cv.waitKey()
cv.destroyAllWindows()
